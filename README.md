Installation 
------------

Télécharger le projet gitlab :

```sh
git clone https://gitlab.com/eliott.wnr/calculatrice.git && cd calculatrice
```

Puis compiler et exécuter le fichier :

```sh
cc -o calculatrice.out fonctions.c calculatrice.c

./calculatrice.out
```
