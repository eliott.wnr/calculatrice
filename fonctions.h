#ifndef FONCTIONS_H
#define FONCTIONS_H

/*déclaration des fonctions de fonctions.c*/

extern int addition(int numberOfTerms, int *terms);   
extern int soustraction(int numberOfTerms, int *terms);    
extern int multiplication(int numberOfTerms, int *terms);    
extern int division(int numberOfTerms, int *terms);    

#endif
