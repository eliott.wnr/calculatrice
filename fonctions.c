#include <stdio.h>
#include <stdlib.h>
#include "fonctions.h"

/*fonctions des calculs*/ 

/*
int addition(int firstNumber, int secondNumber){
	return firstNumber + secondNumber; 
}
*/
int addition(int numberOfTerms, int *terms){
	//addition de tous les termes de terms[]
	int result = 0; 
	for(int count = 0; count < numberOfTerms; count++){
		result += terms[count]; 
	}
	return result; 
}
 
int soustraction(int numberOfTerms, int *terms){
	int result = terms[0]; 
	for(int count = 1; count < numberOfTerms; count++){
		result -= terms[count]; 
	}
	return result; 
}
 
int multiplication(int numberOfTerms, int *terms){
	int result = terms[0]; 
	for(int count = 1; count < numberOfTerms; count++){
		result *= terms[count]; 
	}
	return result; 
} 

int division(int numberOfTerms, int *terms){
	int result = terms[0]; 
	for(int count = 1; count < numberOfTerms; count++){
		if (terms[count] == 0){
			printf("Division par 0 impossible !\n"); 
			exit(EXIT_SUCCESS); 
		}
		result /= terms[count]; 
	}
	return result; 
}
