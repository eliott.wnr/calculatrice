#include <stdio.h>
#include <stdlib.h>
#include "fonctions.h"

/*corps principal du programme*/

int main(){
	int numberOfTerms, result, count; 
	char operator; 

	//prints des choix de l'opération 
	printf("\n************ Calculatrice *************\n"); 
	printf("\n·Addition\t\t·Multiplication\n·Soustraction\t\t·Division\n");
	printf("\nChoisis l'opération avec le symbole qui lui correspond\n"); 

	//type de calcul
	printf("\n--> "); 
	scanf("%c", &operator);

	//demande le nombre de termes
	printf("Nombre de termes : "); 
	scanf("%d", &numberOfTerms); 

	//crée un tableau de longueur numberOfTerms et assigne chaque termes à ce tableau 
	int terms[numberOfTerms]; 
	for(int count = 0; count < numberOfTerms; count++){
		printf("Nombre %d : ", count + 1);
		scanf("%d", &terms[count]);
	}

	//renvoie le calcul a la bonne fonction et print le résultat
	if(operator == '+'){
		result = addition(numberOfTerms, terms); 
	}
	else if(operator == '-'){
		result = soustraction(numberOfTerms, terms); 
	}

	else if(operator == '*'){
		result = multiplication(numberOfTerms, terms); 
	}

	else if(operator == '/'){
		result = division(numberOfTerms, terms); 
	}

	//affiche le résultat
	printf("Le résultat est %d\n", result); 


	return 0; 
}
